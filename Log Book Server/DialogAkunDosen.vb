﻿Imports System.Windows.Forms

Public Class DialogAkunDosen
    Public idUsr As String
    Public mode As String = "add"
    Dim DB As New Connection()
    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        If mode = "add" Then
            If DB.Execute("insert into lb_dosen (`dsn_nidn`,`dsn_nama`) values('" & tbox_nidn.Text & "', '" & tbox_nama.Text & "')") Then
                Me.DialogResult = System.Windows.Forms.DialogResult.OK
                MsgBox("Data Berhasil Disimpan!", vbInformation)
                AkunDosen.Close()
                AkunDosen.Show()

                Me.Close()
            End If
        ElseIf mode = "edit" Then
            If DB.Execute("update lb_dosen set `dsn_nidn`='" & tbox_nidn.Text & "',`dsn_nama`='" & tbox_nama.Text & "' where dsn_nidn = '" & idUsr & "'") Then
                Me.DialogResult = System.Windows.Forms.DialogResult.OK
                MsgBox("Data Berhasil Diubah!", vbInformation)
                AkunDosen.Close()
                AkunDosen.Show()

                Me.Close()
            End If
        End If

    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub DialogAkunDosen_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        AkunDosen.btn_tambah.Enabled = True
        AkunDosen.btn_ubah.Enabled = True
        AkunDosen.btn_hapus.Enabled = True
        AkunDosen.data_users.Enabled = True
    End Sub

    Private Sub DialogTujuanPenggunaan_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If mode = "edit" Then
            Try
                DB.Connect()
                rd = DB.Command("select * from lb_dosen where dsn_nidn = '" & idUsr & "' ").ExecuteReader
                rd.Read()
                If rd.HasRows Then
                    tbox_nidn.Text = rd.Item("dsn_nidn")
                    tbox_nama.Text = rd.Item("dsn_nama")
                Else
                    MsgBox("Data tidak ditemukan :(", MsgBoxStyle.Exclamation)
                End If
            Catch ex As Exception
                MsgBox("Terjadi Kesalahan :( (" & ex.ToString & ")", MsgBoxStyle.Critical)
            End Try
        End If
    End Sub

    Private Sub tbox_nidn_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles tbox_nidn.KeyPress
        If Not Char.IsDigit(e.KeyChar) And Not Char.IsControl(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub


    Private Sub tbox_nidn_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbox_nidn.TextChanged

    End Sub

    Private Sub DialogAkunDosen_LocationChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LocationChanged
        Dim r As Rectangle
        If Parent IsNot Nothing Then
            r = Parent.RectangleToScreen(Parent.ClientRectangle)
        Else
            r = Screen.FromPoint(Location).WorkingArea
        End If

        Dim x = r.Left + (r.Width - Width) \ 2
        Dim y = r.Top + (r.Height - Height) \ 2
        Location = New Point(x, y)
    End Sub
End Class
