﻿Public Class AkunMahasiswa
    Dim DB As New Connection()

    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ShowList()
    End Sub

    Sub ShowList(Optional ByVal query As String = "")
        Try
            DB.Connect()
            cmd = DB.Command("select * from lb_mahasiswa join lb_prodi on lb_prodi.prd_id = lb_mahasiswa.prd_id join lb_fakultas on lb_fakultas.fkt_id = lb_prodi.prd_id where lb_mahasiswa.mhs_nim like '%" & query & "%' or lb_mahasiswa.mhs_nama like '%" & query & "%'")
            rd = cmd.ExecuteReader
            'Membersihkan Semua Baris di DataGridView
            data_users.Rows.Clear()
            Do While rd.Read
                'Menambahkan baris baru pada DataGridView
                data_users.Rows.Add(rd.Item("mhs_nim"), rd.Item("mhs_nama"), rd.Item("prd_name"), rd.Item("fkt_nama"))
            Loop
        Catch ex As Exception
            MsgBox("Terjadi kesalahan :( (" & ex.ToString & ")", MsgBoxStyle.Critical)
        End Try
    End Sub
    Private Sub LihatToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LihatToolStripMenuItem.Click
        Main.Show()
        Close()
    End Sub

    Private Sub LaporanToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LaporanToolStripMenuItem.Click
        Laporan.Show()
        Close()
    End Sub

    Private Sub AdminToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        AkunAdmin.Show()
        Close()
    End Sub


    Private Sub AdminToolStripMenuItem_Click_2(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AdminToolStripMenuItem.Click
        AkunAdmin.Show()
        Close()
    End Sub

    Private Sub MahasiswaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MahasiswaToolStripMenuItem.Click
        'AkunMahasiswa.Show()
        'Close()
    End Sub

    Private Sub DosenToolStripMenuItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DosenToolStripMenuItem.Click
        AkunDosen.Show()
        Close()
    End Sub

    Private Sub TamuToolStripMenuItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TamuToolStripMenuItem.Click
        AkunTamu.Show()
        Close()
    End Sub

    Private Sub TujuanPenggunaanToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TujuanPenggunaanToolStripMenuItem.Click
        PengaturanTujuanPenggunaan.Show()
        Close()
    End Sub

    Private Sub KeluarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles KeluarToolStripMenuItem.Click
        Dim answer As DialogResult
        answer = MessageBox.Show("Apakah Anda Yakin Ingin Keluar?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If answer = vbYes Then
            auth_data.Clear()
            auth_user = ""
            Login.Show()
            Close()
        End If
    End Sub
    Private Sub cbox_tahun_ajaran_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ShowList()
    End Sub

    Private Sub btn_tambah_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tambah.Click
        btn_tambah.Enabled = False
        btn_ubah.Enabled = False
        btn_hapus.Enabled = False
        btnImportData.Enabled = False
        data_users.Enabled = False
        DialogAkunMahasiswa.Show()
    End Sub

    Private Sub btn_ubah_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_ubah.Click
        btn_tambah.Enabled = False
        btn_ubah.Enabled = False
        btn_hapus.Enabled = False
        btnImportData.Enabled = False
        data_users.Enabled = False
        DialogAkunMahasiswa.idUsr = data_users.SelectedRows.Item(0).Cells(0).Value
        DialogAkunMahasiswa.fktName = data_users.SelectedRows.Item(0).Cells(3).Value
        DialogAkunMahasiswa.prdName = data_users.SelectedRows.Item(0).Cells(2).Value
        DialogAkunMahasiswa.mode = "edit"
        DialogAkunMahasiswa.Show()
    End Sub

    Private Sub btn_hapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_hapus.Click
        Dim answer As DialogResult
        answer = MessageBox.Show("Apakah Anda Yakin?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If answer = vbYes Then
            If DB.Execute("delete from lb_mahasiswa where mhs_nim = '" & data_users.SelectedRows.Item(0).Cells(0).Value & "'") Then
                MsgBox("Data Berhasil Dihapus!", vbInformation)
                ShowList()
            End If
        End If
        
    End Sub

    Private Sub GroupBox1_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupBox1.Enter

    End Sub

    Private Sub tbox_pencarian_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbox_pencarian.TextChanged
        ShowList(tbox_pencarian.Text)
    End Sub

    Private Sub btnImportData_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImportData.Click
        btn_tambah.Enabled = False
        btn_ubah.Enabled = False
        btn_hapus.Enabled = False
        btnImportData.Enabled = False
        data_users.Enabled = False
        DialogImportData.Show()
    End Sub
End Class