﻿Imports System.Windows.Forms

Public Class DialogAkunTamu
    Public idUsr As String
    Public mode As String = "add"
    Dim DB As New Connection()
    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        If mode = "add" Then
            If DB.Execute("insert into lb_tamu (`tmu_code`,`tmu_nama`) values('" & tbox_code.Text & "', '" & tbox_nama.Text & "')") Then
                Me.DialogResult = System.Windows.Forms.DialogResult.OK
                MsgBox("Data Berhasil Disimpan!", vbInformation)
                AkunTamu.Close()
                AkunTamu.Show()

                Me.Close()
            End If
        ElseIf mode = "edit" Then
            If DB.Execute("update lb_tamu set `tmu_code`='" & tbox_code.Text & "',`tmu_nama`='" & tbox_nama.Text & "' where tmu_code = '" & idUsr & "'") Then
                Me.DialogResult = System.Windows.Forms.DialogResult.OK
                MsgBox("Data Berhasil Diubah!", vbInformation)
                AkunTamu.Close()
                AkunTamu.Show()

                Me.Close()
            End If
        End If

    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub DialogAkunTamu_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        AkunTamu.btn_tambah.Enabled = True
        AkunTamu.btn_ubah.Enabled = True
        AkunTamu.btn_hapus.Enabled = True
        AkunTamu.data_users.Enabled = True
    End Sub

    Private Sub DialogTujuanPenggunaan_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If mode = "edit" Then
            Try
                DB.Connect()
                rd = DB.Command("select * from lb_tamu where tmu_code = '" & idUsr & "' ").ExecuteReader
                rd.Read()
                If rd.HasRows Then
                    tbox_code.Text = rd.Item("tmu_code")
                    tbox_nama.Text = rd.Item("tmu_nama")
                Else
                    MsgBox("Data tidak ditemukan :(", MsgBoxStyle.Exclamation)
                End If
            Catch ex As Exception
                MsgBox("Terjadi Kesalahan :( (" & ex.ToString & ")", MsgBoxStyle.Critical)
            End Try
        End If
    End Sub


    Private Sub DialogAkunTamu_LocationChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LocationChanged
        Dim r As Rectangle
        If Parent IsNot Nothing Then
            r = Parent.RectangleToScreen(Parent.ClientRectangle)
        Else
            r = Screen.FromPoint(Location).WorkingArea
        End If

        Dim x = r.Left + (r.Width - Width) \ 2
        Dim y = r.Top + (r.Height - Height) \ 2
        Location = New Point(x, y)
    End Sub
End Class
