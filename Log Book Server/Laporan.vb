﻿Imports MySql.Data.MySqlClient

Public Class Laporan
    Dim DB As New Connection()
    Dim adp As New MySqlDataAdapter
    Dim dt As New DataSet
    Private MyKoneksi As New MySqlConnection(My.Settings.defaultConnection)

    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim sql As String
        sql = "select p.pgn_id,p.pgn_mulai,p.pgn_selesai,t.tjn_kegiatan,t.tjn_penanggung_jawab from lb_pengguna p join lb_tujuan t on t.tjn_id = p.tjn_id where p.pgn_selesai != '0000-00-00 00:00:00'"
            MyKoneksi.Open()
            Dim rpt As New LogBook() 'Report yang telah dibuat.
            Dim oDs As New DataSet() 'Dataaset yang telah dibuat.
            Try
                Dim oDa As New MySqlDataAdapter(sql, MyKoneksi)
            oDa.Fill(oDs, "lb_pengguna")
            oDa.Fill(oDs, "lb_tujuan")
                rpt.SetDataSource(oDs)
            CrystalReportViewer1.ReportSource = rpt
            Catch Excep As Exception
                MessageBox.Show(Excep.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
            MyKoneksi.Close()
    End Sub

    Private Sub LihatToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LihatToolStripMenuItem.Click
        Main.Show()
        Close()
    End Sub

    Private Sub LaporanToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LaporanToolStripMenuItem.Click
        'Laporan.Show()
        'Close()
    End Sub

    Private Sub AdminToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        AkunAdmin.Show()
        Close()
    End Sub


    Private Sub AdminToolStripMenuItem_Click_2(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AdminToolStripMenuItem.Click
        AkunAdmin.Show()
        Close()
    End Sub

    Private Sub MahasiswaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MahasiswaToolStripMenuItem.Click
        AkunMahasiswa.Show()
        Close()
    End Sub

    Private Sub DosenToolStripMenuItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DosenToolStripMenuItem.Click
        AkunDosen.Show()
        Close()
    End Sub

    Private Sub TamuToolStripMenuItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TamuToolStripMenuItem.Click
        AkunTamu.Show()
        Close()
    End Sub

    Private Sub TujuanPenggunaanToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TujuanPenggunaanToolStripMenuItem.Click
        PengaturanTujuanPenggunaan.Show()
        Close()
    End Sub

    Private Sub KeluarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles KeluarToolStripMenuItem.Click
        Dim answer As DialogResult
        answer = MessageBox.Show("Apakah Anda Yakin Ingin Keluar?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If answer = vbYes Then
            auth_data.Clear()
            auth_user = ""
            Login.Show()
            Close()
        End If
    End Sub

    Private Sub cbox_tahun_ajaran_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbox_tahun_ajaran.ValueChanged
        Dim sql As String
        sql = "select p.pgn_id,p.pgn_mulai,p.pgn_selesai,t.tjn_kegiatan,t.tjn_penanggung_jawab,t.tjn_tahun_ajaran from lb_pengguna p join lb_tujuan t on t.tjn_id = p.tjn_id where t.tjn_tahun_ajaran = '" & cbox_tahun_ajaran.Text & "' and p.pgn_selesai != '0000-00-00 00:00:00'"
        MyKoneksi.Open()
        Dim rpt As New LogBook() 'Report yang telah dibuat.
        Dim oDs As New DataSet() 'Dataaset yang telah dibuat.
        Try
            Dim oDa As New MySqlDataAdapter(sql, MyKoneksi)
            oDa.Fill(oDs, "lb_pengguna")
            oDa.Fill(oDs, "lb_tujuan")
            rpt.SetDataSource(oDs)
            CrystalReportViewer1.ReportSource = rpt
        Catch Excep As Exception
            MessageBox.Show(Excep.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
        MyKoneksi.Close()
    End Sub
End Class