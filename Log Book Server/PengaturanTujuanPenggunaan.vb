﻿Public Class PengaturanTujuanPenggunaan
    Dim DB As New Connection()

    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ShowList()
    End Sub

    Sub ShowList(Optional ByVal query As String = "")
        Try
            DB.Connect()
            If query <> "" Then
                cmd = DB.Command("select * from lb_tujuan where tjn_kegiatan like '%" & query & "%' AND tjn_tahun_ajaran = '" & cbox_tahun_ajaran.Value & "' ")
            Else
                cmd = DB.Command("select * from lb_tujuan where tjn_tahun_ajaran = '" & cbox_tahun_ajaran.Value & "'")
            End If

            rd = cmd.ExecuteReader
            'Membersihkan Semua Baris di DataGridView
            data_tujuan.Rows.Clear()
            Do While rd.Read
                'Menambahkan baris baru pada DataGridView
                data_tujuan.Rows.Add(rd.Item("tjn_id"), rd.Item("tjn_kegiatan"), rd.Item("tjn_penanggung_jawab"))
            Loop
        Catch ex As Exception
            'MsgBox("Terjadi kesalahan :( (" & ex.ToString & ")", MsgBoxStyle.Critical)
        End Try
    End Sub
    Private Sub LihatToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LihatToolStripMenuItem.Click
        Main.Show()
        Close()
    End Sub

    Private Sub LaporanToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LaporanToolStripMenuItem.Click
        Laporan.Show()
        Close()
    End Sub

    Private Sub AdminToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        AkunAdmin.Show()
        Close()
    End Sub


    Private Sub AdminToolStripMenuItem_Click_2(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AdminToolStripMenuItem.Click
        AkunAdmin.Show()
        Close()
    End Sub

    Private Sub MahasiswaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MahasiswaToolStripMenuItem.Click
        AkunMahasiswa.Show()
        Close()
    End Sub

    Private Sub DosenToolStripMenuItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DosenToolStripMenuItem.Click
        AkunDosen.Show()
        Close()
    End Sub

    Private Sub TamuToolStripMenuItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TamuToolStripMenuItem.Click
        AkunTamu.Show()
        Close()
    End Sub

    Private Sub TujuanPenggunaanToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TujuanPenggunaanToolStripMenuItem.Click
        'PengaturanTujuanPenggunaan.Show()
        'Close()
    End Sub

    Private Sub KeluarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles KeluarToolStripMenuItem.Click
        Dim answer As DialogResult
        answer = MessageBox.Show("Apakah Anda Yakin Ingin Keluar?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If answer = vbYes Then
            auth_data.Clear()
            auth_user = ""
            Login.Show()
            Close()
        End If
    End Sub

    Private Sub cbox_tahun_ajaran_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbox_tahun_ajaran.ValueChanged
        ShowList()
    End Sub

    Private Sub btn_tambah_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tambah.Click
        btn_tambah.Enabled = False
        btn_ubah.Enabled = False
        btn_hapus.Enabled = False
        data_tujuan.Enabled = False
        DialogTujuanPenggunaan.Show()
    End Sub

    Private Sub btn_ubah_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_ubah.Click
        btn_tambah.Enabled = False
        btn_ubah.Enabled = False
        btn_hapus.Enabled = False
        data_tujuan.Enabled = False
        DialogTujuanPenggunaan.idTjn = data_tujuan.SelectedRows.Item(0).Cells(0).Value
        DialogTujuanPenggunaan.mode = "edit"
        DialogTujuanPenggunaan.Show()
    End Sub

    Private Sub btn_hapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_hapus.Click
        Dim answer As DialogResult
        answer = MessageBox.Show("Apakah Anda Yakin?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If answer = vbYes Then
            If DB.Execute("delete from lb_tujuan where tjn_id = '" & data_tujuan.SelectedRows.Item(0).Cells(0).Value & "'") Then
                ShowList()
            End If
        End If

    End Sub

    Private Sub tbox_pencarian_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbox_pencarian.TextChanged
        ShowList(tbox_pencarian.Text)
    End Sub

    Private Sub btn_kembali_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
End Class