﻿Imports System.Windows.Forms
Imports System.Data.OleDb

Public Class DialogImportData
    Dim DB As New Connection()
    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
            Dim I As Integer
        For I = 0 To DGV.Rows.Count - 1
            Dim row As DataGridViewRow = DGV.Rows(I)
            Dim nim As DataGridViewTextBoxCell = row.Cells(0)
            Dim prd As DataGridViewTextBoxCell = row.Cells(1)
            Dim nama As DataGridViewTextBoxCell = row.Cells(2)
            'MsgBox(nim.Value & " | " & prd.Value & " | " & nama.Value)
            DB.Connect()
            If DB.Execute("insert into lb_mahasiswa values ('" & nim.Value & "', '" & prd.Value & "', '" & nama.Value & "')") Then
                Me.DialogResult = System.Windows.Forms.DialogResult.OK
            End If
        Next
        MsgBox("Data Berhasil Disimpan!", vbInformation)
        AkunMahasiswa.Close()
        AkunMahasiswa.Show()
        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        AkunMahasiswa.Close()
        AkunMahasiswa.Show()
        Me.Close()
    End Sub

    Private Sub DialogImportData_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        AkunMahasiswa.btn_tambah.Enabled = True
        AkunMahasiswa.btn_ubah.Enabled = True
        AkunMahasiswa.btn_hapus.Enabled = True
        AkunMahasiswa.data_users.Enabled = True
    End Sub

    Private Sub DialogImportData_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Dim conn As OleDbConnection
    Dim da As OleDbDataAdapter
    Dim ds As New DataSet
    Dim cmd As OleDbCommand

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        On Error Resume Next
        openfiledialog1.Filter = "(*.xls)|*.xls|(*.xlsx)|*.xlsx|All files (*.*)|*.*"
        OpenFileDialog1.ShowDialog()

        conn = New OleDbConnection("provider=Microsoft.Jet.OLEDB.4.0;" & _
                    "data source='" & OpenFileDialog1.FileName & "';Extended Properties=Excel 8.0;")

        da = New OleDbDataAdapter("select * from [Sheet1$]", conn)
        conn.Open()
        ds.Clear()
        MsgBox(ds.Tables(0))
        da.Fill(ds)
        DGV.DataSource = ds.Tables(0)
        MsgBox(ds.Tables(0).Rows(0))
        conn.Close()
    End Sub

    Private Sub DialogImportData_LocationChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LocationChanged
        Dim r As Rectangle
        If Parent IsNot Nothing Then
            r = Parent.RectangleToScreen(Parent.ClientRectangle)
        Else
            r = Screen.FromPoint(Location).WorkingArea
        End If

        Dim x = r.Left + (r.Width - Width) \ 2
        Dim y = r.Top + (r.Height - Height) \ 2
        Location = New Point(x, y)
    End Sub
End Class
