﻿Imports System.Windows.Forms

Public Class DialogTujuanPenggunaan
    Public idTjn As String
    Public mode As String = "add"
    Dim DB As New Connection()
    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        If mode = "add" Then
            If DB.Execute("insert into lb_tujuan (`tjn_id`,`tjn_kegiatan`,`tjn_tahun_ajaran`,`tjn_penanggung_jawab`) values('', '" & tbox_kegiatan.Text & "', '" & cbox_tahun_ajaran.Text & "', '" & tbox_penangung_jawab.Text & "')") Then
                Me.DialogResult = System.Windows.Forms.DialogResult.OK
                MsgBox("Data Berhasil Disimpan!", vbInformation)
                PengaturanTujuanPenggunaan.Close()
                PengaturanTujuanPenggunaan.Show()

                Me.Close()
            End If
        ElseIf mode = "edit" Then
            If DB.Execute("update lb_tujuan set `tjn_kegiatan`='" & tbox_kegiatan.Text & "',`tjn_tahun_ajaran`='" & cbox_tahun_ajaran.Text & "',`tjn_penanggung_jawab`='" & tbox_penangung_jawab.Text & "' where tjn_id = '" & idTjn & "'") Then
                Me.DialogResult = System.Windows.Forms.DialogResult.OK
                MsgBox("Data Berhasil Diubah!", vbInformation)
                PengaturanTujuanPenggunaan.Close()
                PengaturanTujuanPenggunaan.Show()

                Me.Close()
            End If
        End If

    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub DialogTujuanPenggunaan_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        PengaturanTujuanPenggunaan.btn_tambah.Enabled = True
        PengaturanTujuanPenggunaan.btn_ubah.Enabled = True
        PengaturanTujuanPenggunaan.btn_hapus.Enabled = True
        PengaturanTujuanPenggunaan.data_tujuan.Enabled = True
    End Sub

    Private Sub DialogTujuanPenggunaan_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If mode = "edit" Then
            Try
                DB.Connect()
                rd = DB.Command("select * from lb_tujuan where tjn_id = '" & idTjn & "' ").ExecuteReader
                rd.Read()
                If rd.HasRows Then
                    tbox_kegiatan.Text = rd.Item("tjn_kegiatan")
                    tbox_penangung_jawab.Text = rd.Item("tjn_penanggung_jawab")
                    cbox_tahun_ajaran.Text = rd.Item("tjn_tahun_ajaran")
                Else
                    MsgBox("Data tidak ditemukan :(", MsgBoxStyle.Exclamation)
                End If
            Catch ex As Exception
                MsgBox("Terjadi Kesalahan :( (" & ex.ToString & ")", MsgBoxStyle.Critical)
            End Try
        End If
    End Sub

    Private Sub DialogTujuanPenggunaan_LocationChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LocationChanged
        Dim r As Rectangle
        If Parent IsNot Nothing Then
            r = Parent.RectangleToScreen(Parent.ClientRectangle)
        Else
            r = Screen.FromPoint(Location).WorkingArea
        End If

        Dim x = r.Left + (r.Width - Width) \ 2
        Dim y = r.Top + (r.Height - Height) \ 2
        Location = New Point(x, y)
    End Sub
End Class
