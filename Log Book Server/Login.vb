﻿Public Class Login
    Dim DB As New Connection
    Private Sub btn_masuk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_masuk.Click
        Masuk()
    End Sub

    Sub Masuk()
        If tbox_username.Text <> "" And tbox_password.Text <> "" Then
            Try
                DB.Connect()
                rd = DB.Command("select * from lb_users where usr_username = '" & tbox_username.Text & "' && usr_password = '" & tbox_password.Text & "'").ExecuteReader
                rd.Read()
                If rd.HasRows Then
                    auth_user = rd.Item("usr_id")
                    auth_data.Add("id", rd.Item("usr_id"))
                    auth_data.Add("role", rd.Item("usr_status"))

                    Main.Show()
                    Close()
                Else
                    MsgBox("Username atau password salah :(", MsgBoxStyle.Exclamation)
                End If
            Catch ex As Exception
                MsgBox("Terjadi Kesalahan :( (" & ex.ToString & ")", MsgBoxStyle.Critical)
            End Try
        Else
            MsgBox("Masukan data yang diminta!", MsgBoxStyle.Exclamation)
        End If
    End Sub



    Private Sub Login_LocationChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LocationChanged
        Dim r As Rectangle
        If Parent IsNot Nothing Then
            r = Parent.RectangleToScreen(Parent.ClientRectangle)
        Else
            r = Screen.FromPoint(Location).WorkingArea
        End If

        Dim x = r.Left + (r.Width - Width) \ 2
        Dim y = r.Top + (r.Height - Height) \ 2
        Location = New Point(x, y)
    End Sub

    Private Sub tbox_password_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles tbox_password.KeyDown
        If e.KeyCode = Keys.Enter Then
            Masuk()
        End If
    End Sub
End Class
