﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Main
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.MenuList = New System.Windows.Forms.MenuStrip()
        Me.LihatToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LaporanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PengaturanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TujuanPenggunaanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.KeluarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.tbox_pencarian = New System.Windows.Forms.TextBox()
        Me.data_pengguna = New System.Windows.Forms.DataGridView()
        Me.Tanggal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NoPc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NIM = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Nama = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Tujuan = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Mulai = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Selesai = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Tahun = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Riwayat = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AkunToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PenggunaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MahasiswaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DosenToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TamuToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AdminToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuList.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.data_pengguna, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MenuList
        '
        Me.MenuList.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.LihatToolStripMenuItem, Me.LaporanToolStripMenuItem, Me.PengaturanToolStripMenuItem, Me.KeluarToolStripMenuItem})
        Me.MenuList.Location = New System.Drawing.Point(0, 0)
        Me.MenuList.Name = "MenuList"
        Me.MenuList.Size = New System.Drawing.Size(984, 24)
        Me.MenuList.TabIndex = 1
        Me.MenuList.Text = "Menu"
        '
        'LihatToolStripMenuItem
        '
        Me.LihatToolStripMenuItem.Name = "LihatToolStripMenuItem"
        Me.LihatToolStripMenuItem.Size = New System.Drawing.Size(129, 20)
        Me.LihatToolStripMenuItem.Text = "Lihat Data Pengguna"
        '
        'LaporanToolStripMenuItem
        '
        Me.LaporanToolStripMenuItem.Name = "LaporanToolStripMenuItem"
        Me.LaporanToolStripMenuItem.Size = New System.Drawing.Size(62, 20)
        Me.LaporanToolStripMenuItem.Text = "Laporan"
        '
        'PengaturanToolStripMenuItem
        '
        Me.PengaturanToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TujuanPenggunaanToolStripMenuItem, Me.AkunToolStripMenuItem})
        Me.PengaturanToolStripMenuItem.Name = "PengaturanToolStripMenuItem"
        Me.PengaturanToolStripMenuItem.Size = New System.Drawing.Size(80, 20)
        Me.PengaturanToolStripMenuItem.Text = "Pengaturan"
        '
        'TujuanPenggunaanToolStripMenuItem
        '
        Me.TujuanPenggunaanToolStripMenuItem.Name = "TujuanPenggunaanToolStripMenuItem"
        Me.TujuanPenggunaanToolStripMenuItem.Size = New System.Drawing.Size(181, 22)
        Me.TujuanPenggunaanToolStripMenuItem.Text = "Tujuan Penggunaan"
        '
        'KeluarToolStripMenuItem
        '
        Me.KeluarToolStripMenuItem.Name = "KeluarToolStripMenuItem"
        Me.KeluarToolStripMenuItem.Size = New System.Drawing.Size(52, 20)
        Me.KeluarToolStripMenuItem.Text = "Keluar"
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.tbox_pencarian)
        Me.GroupBox1.Controls.Add(Me.data_pengguna)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 27)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(960, 422)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Data Pengguna Komputer"
        '
        'tbox_pencarian
        '
        Me.tbox_pencarian.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tbox_pencarian.Location = New System.Drawing.Point(735, 35)
        Me.tbox_pencarian.Name = "tbox_pencarian"
        Me.tbox_pencarian.Size = New System.Drawing.Size(182, 20)
        Me.tbox_pencarian.TabIndex = 1
        '
        'data_pengguna
        '
        Me.data_pengguna.AllowUserToAddRows = False
        Me.data_pengguna.AllowUserToDeleteRows = False
        Me.data_pengguna.AllowUserToOrderColumns = True
        Me.data_pengguna.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.data_pengguna.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.data_pengguna.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle9
        Me.data_pengguna.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.data_pengguna.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Tanggal, Me.NoPc, Me.NIM, Me.Nama, Me.Tujuan, Me.Mulai, Me.Selesai, Me.Tahun, Me.Riwayat})
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.data_pengguna.DefaultCellStyle = DataGridViewCellStyle10
        Me.data_pengguna.Location = New System.Drawing.Point(45, 75)
        Me.data_pengguna.Name = "data_pengguna"
        Me.data_pengguna.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.data_pengguna.Size = New System.Drawing.Size(872, 331)
        Me.data_pengguna.TabIndex = 0
        '
        'Tanggal
        '
        Me.Tanggal.FillWeight = 50.0!
        Me.Tanggal.HeaderText = "Tanggal"
        Me.Tanggal.Name = "Tanggal"
        '
        'NoPc
        '
        Me.NoPc.FillWeight = 30.0!
        Me.NoPc.HeaderText = "No PC"
        Me.NoPc.Name = "NoPc"
        '
        'NIM
        '
        Me.NIM.FillWeight = 40.0!
        Me.NIM.HeaderText = "NIM"
        Me.NIM.Name = "NIM"
        '
        'Nama
        '
        Me.Nama.HeaderText = "Nama"
        Me.Nama.Name = "Nama"
        '
        'Tujuan
        '
        Me.Tujuan.HeaderText = "Tujuan Penggunaan"
        Me.Tujuan.Name = "Tujuan"
        '
        'Mulai
        '
        Me.Mulai.FillWeight = 50.0!
        Me.Mulai.HeaderText = "Jam Mulai"
        Me.Mulai.Name = "Mulai"
        '
        'Selesai
        '
        Me.Selesai.FillWeight = 50.0!
        Me.Selesai.HeaderText = "Jam Selesai"
        Me.Selesai.Name = "Selesai"
        '
        'Tahun
        '
        Me.Tahun.FillWeight = 40.0!
        Me.Tahun.HeaderText = "Tahun Ajaran"
        Me.Tahun.Name = "Tahun"
        '
        'Riwayat
        '
        Me.Riwayat.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Riwayat.FillWeight = 200.0!
        Me.Riwayat.HeaderText = "Aplikasi Yang Dibuka"
        Me.Riwayat.Name = "Riwayat"
        '
        'AkunToolStripMenuItem
        '
        Me.AkunToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AdminToolStripMenuItem, Me.PenggunaToolStripMenuItem})
        Me.AkunToolStripMenuItem.Name = "AkunToolStripMenuItem"
        Me.AkunToolStripMenuItem.Size = New System.Drawing.Size(181, 22)
        Me.AkunToolStripMenuItem.Text = "Akun"
        '
        'PenggunaToolStripMenuItem
        '
        Me.PenggunaToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MahasiswaToolStripMenuItem, Me.DosenToolStripMenuItem, Me.TamuToolStripMenuItem})
        Me.PenggunaToolStripMenuItem.Name = "PenggunaToolStripMenuItem"
        Me.PenggunaToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.PenggunaToolStripMenuItem.Text = "Pengguna"
        '
        'MahasiswaToolStripMenuItem
        '
        Me.MahasiswaToolStripMenuItem.Name = "MahasiswaToolStripMenuItem"
        Me.MahasiswaToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.MahasiswaToolStripMenuItem.Text = "Mahasiswa"
        '
        'DosenToolStripMenuItem
        '
        Me.DosenToolStripMenuItem.Name = "DosenToolStripMenuItem"
        Me.DosenToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.DosenToolStripMenuItem.Text = "Dosen"
        '
        'TamuToolStripMenuItem
        '
        Me.TamuToolStripMenuItem.Name = "TamuToolStripMenuItem"
        Me.TamuToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.TamuToolStripMenuItem.Text = "Tamu"
        '
        'AdminToolStripMenuItem
        '
        Me.AdminToolStripMenuItem.Name = "AdminToolStripMenuItem"
        Me.AdminToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.AdminToolStripMenuItem.Text = "Admin"
        '
        'Main
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(984, 461)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.MenuList)
        Me.MainMenuStrip = Me.MenuList
        Me.Name = "Main"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Data Pengguna"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.MenuList.ResumeLayout(False)
        Me.MenuList.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.data_pengguna, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuList As System.Windows.Forms.MenuStrip
    Friend WithEvents LihatToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LaporanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PengaturanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TujuanPenggunaanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents data_pengguna As System.Windows.Forms.DataGridView
    Friend WithEvents tbox_pencarian As System.Windows.Forms.TextBox
    Friend WithEvents KeluarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Tanggal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NoPc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NIM As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Nama As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Tujuan As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Mulai As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Selesai As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Tahun As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Riwayat As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AkunToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AdminToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PenggunaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MahasiswaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DosenToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TamuToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
End Class
