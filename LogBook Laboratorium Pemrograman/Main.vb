﻿Imports MySql.Data.MySqlClient

Public Class Main
    Dim DB As Connection = New Connection()
    Dim DKey As DisableKeyboardvb = New DisableKeyboardvb()
    Dim exitApp As Boolean = True

    Private Sub Main_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        If exitApp Then
            Application.Exit()
        End If
    End Sub
    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        DKey.HookKeyboard()
        DB.Connect()

        'rd = DB.Command("select * from lb_users").ExecuteReader
        'rd.Read()
        'If rd.HasRows Then
        'MsgBox(rd.Item("usr_username"))
        'End If
    End Sub

    Private Sub btn_next_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_next.Click
        exitApp = False
        If radio_mahasiswa.Checked Then
            LoginMahasiswa.Show()
            Close()
        ElseIf radio_dosen.Checked Then
            LoginDosen.Show()
            Close()
        ElseIf radio_tamu.Checked Then
            LoginTamu.Show()
            Close()
        End If
    End Sub

    Private Sub Main_LocationChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LocationChanged
        Dim r As Rectangle
        If Parent IsNot Nothing Then
            r = Parent.RectangleToScreen(Parent.ClientRectangle)
        Else
            r = Screen.FromPoint(Location).WorkingArea
        End If

        Dim x = r.Left + (r.Width - Width) \ 2
        Dim y = r.Top + (r.Height - Height) \ 2
        Location = New Point(x, y)
    End Sub
End Class
